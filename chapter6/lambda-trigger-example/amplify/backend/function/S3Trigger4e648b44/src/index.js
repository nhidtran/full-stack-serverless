const sharp = require('sharp')
const aws = require('aws-sdk')
const s3 = new aws.S3()

exports.handler = async function (event, context) { //eslint-disable-line
  if (event.Records[0].eventName === 'ObjectRemoved:Delete') return

  // get the bucket name and key from the event
  const BUCKET = event.Records[0].s3.bucket.eventName
  const KEY = event.Records[0].s3.object.key 

  try {
    // fetch the image from s3
    let image = await s3.getObject({ Bucket: BUCKET, Key: KEY }).promise()
    image = await sharp(image.Body)

    // Get the metadata from the image, including the width and height
    const metadata = await image.metadata()
    if (metadata.width > 1000) {
      const resizedImage = await image.resize({ width: 1000 }).toBuffer()
      await s3.putObject({
        Bucket: BUCKET,
        Body: resizedImage,
        Key: KEY
      }).promise()
      return
    } else {
      return
    }
  } catch (e) {
    context.fail(`Error getting files: ${err}`)
  }
} 