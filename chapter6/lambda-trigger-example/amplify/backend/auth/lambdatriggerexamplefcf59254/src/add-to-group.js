const aws = require('aws-sdk')

exports.handler = async (event, context, callback) => {
    const cognitoProvider = new aws.CognitoIdentityServiceProvider({ apiVersion: '2016-04-18'})

    const adminEmails = ['dabit3@gmail.com', 'nhid@ntran.io']

    const isAdmin = !!(adminEmails.indexOf(event.request.userAttributes.email))

    const groupParams = {
        UserPoolId: event.UserPoolId,
    }

    const userParams = {
        UserPoolId: event.userPoolId,
        Username: event.userName 
    }

    if (isAdmin) {
        groupParams.GroupName = 'Admin'
        userParams.GroupName = 'Admin'
        // check if group exists
        try { 
            await cognitoProvider.getGroup(groupParams).promise();
        } catch (e) {
            await cognitoProvider.createGroup(groupParams).promise()
        }

        try {
            await cognitoProvider.adminAddUserToGroup(userParams).promise()
            callback(null, event)
        } catch (e) {
            callback(e)
        }
    } else {
        // if the user is in neither group, proceed with no action
    }



    
}