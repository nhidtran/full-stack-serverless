import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Auth, Storage } from 'aws-amplify'
import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react'
import { v4 as uuid } from 'uuid' 

function App () {
  const [user, updateUser] = React.useState(null);
  const [images, setImages] = React.useState([]);

  React.useEffect(() => {
    Auth.currentAuthenticatedUser()
    .then(user => updateUser(user))
    .catch(err => {
      debugger;
      console.log(err)
    })
  }, [])

  React.useEffect(() => {
    fetchImages()
  }, [])

  async function onChange(e) {
    // when a file is uploaded, create a unique name and save it using Storage API
    const file = e.target.files[0]
    const filetype = file.name.split('.')[file.name.split.length - 1]

    await Storage.put(`${uuid()}.${filetype}`, file)
    fetchImages()
  }

  async function fetchImages() {
    const files = await Storage.list('')
    const signedFiles = await Promise.all(files.map(async file => {
      // to sign the images, map over image key array and get signed url
      const signedFile = await Storage.get(file.key)
      return signedFile
    }))
    console.log('%csignedFiles...:', 'color:hotpink', signedFiles)
    console.log('%cfiles...:', 'color:hotpink', files)
    setImages(signedFiles)
  }

  let isAdmin = false;

  if (user) {
    const { signInUserSession: { idToken: { payload }}} = user
    console.log('%cPAYLOAD:', 'color:yellow', payload)
    if (payload['cognito:groups'] && payload['cognito:groups'].includes('Admin')) {
      isAdmin = true
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <input
          type="file"
          onChange={onChange}
        />
        {images.map(image => (
          <img
            src={image}
            key={image}
            style={{width: 500 }}
          />
        ))}
      </header>
    </div>
  )
}
export default withAuthenticator(App)