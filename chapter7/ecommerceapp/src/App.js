import React from 'react';
import logo from './logo.svg';
import './App.css';
import { HashRouter, Route, Switch } from 'react-router-dom'
import { API, Auth, container, Hub, input } from 'aws-amplify';
import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react'
import { Link } from 'react-router-dom';
import { Button, Input, Menu } from 'antd'
import { HomeOutlined, UserOutlined, ProfileOutlined } from '@ant-design/icons'
import { List } from 'antd/lib/form/Form';


export async function checkUser(updateUser) {
  const userData = await Auth
  .currentSession()
  .catch(err => console.log('error: ', err))
  if (!userData) {
    debugger;
    console.log('user data:', userData)
    updateUser({})
    return
  }
  const { idToken: { payload }} = userData
  debugger;
  const isAuthorized = payload['cognito:groups'] && payload['cognito:groups'].includes('Admin')
  updateUser({
    username: payload['cognito:username'],
    isAuthorized
  })
}

const containerStyle = {
  width: 900,
  margin: '0 auto',
  padding: '20px 0px'
};

export function Container({ children }) {
  return (
    <div style={containerStyle}>
      {children}
    </div>
  )
}


export function Router () {
  const [current, setCurrent] = React.useState('home')


  React.useEffect(() => {
    setRoute()
    window.addEventListener('hashchange', setRoute)
    return () => {
      window.removeEventListener('hashchange', setRoute)
    }
  }, [])

  function setRoute () {
    const location = window.location.href.split('/')
    const pathname = location[location.length - 1]
    console.log('pathname:', pathname);
    setCurrent(pathname ? pathname : 'home')
  }
  return (
    <HashRouter>
      <Nav current={current} />
      <Switch>
        <Route exact path='/' component={MainComponent} />
        <Route path='/admin' component={Admin} />
        <Route path='/profile' component={Profile} />
        <Route component={MainComponent} />
      </Switch>
    </HashRouter>
  )
}

const profileStyle = {
  width: 400,
  margin: '20px auto'
}
export const Profile = () => {
  // if signed in, render the component
  return (
    withAuthenticator(
      <div style={profileStyle}>
        <AmplifySignOut />
      </div>
      )
  )
}

export const Nav = (props) => {
  const { current } = props;
  const [user, updateUser] = React.useState({})
  React.useEffect(() => {
    checkUser(updateUser)
    // Hub component listns to auth events (singin and signout) and invoke check user to keep nav state up to date
    Hub.listen('auth', data => {
      const { payload: { event } } = data;
      console.log('event:', event)
      if (event === 'signIn' || event === 'signOut') {
        checkUser(updateUser)
      }
    })
  }, [])

  return (
    <Menu selectedKeys={[current]} mode="horizontal">
      <Menu.Item key='home'>
        <Link to={`/`}>
          <HomeOutlined />home
        </Link>
      </Menu.Item>
      <Menu.Item key='profile'>
        <Link to='/profile'>
          <UserOutlined />Profile
        </Link>
      </Menu.Item>
      {
        user.isAuthorized && (
          <Menu.Item key='admin'>
            <Link to="/admin">
              <ProfileOutlined />Admin
            </Link>
          </Menu.Item>
        )
      }
    </Menu>
  )
}


const initialState = { name: '', price: '' }
const adminStyle = {
  width: 400,
  margin: '20px auto'
}
const inputStyle = { marginTop: 10 }
const buttonStyle = { marginTop: 10 }
export function Admin () {
  // create new items in inventory
  const [itemInfo, updateItemInfo] = React.useState(initialState)
  function updateForm(e) {
    const formData = {
      ...itemInfo, [e.target.name]: e.target.value
    }
    updateItemInfo(formData)
  }
  async function addItem() {
    try {
      const data = {
        body: {...itemInfo, price: parseInt(itemInfo.price)}
      }
      updateItemInfo(initialState)
      await API.post('ecommerceapi', '/products', data)
    } catch (e) {
      console.log('%cerror adding item...', 'color:pink', );
    }
  }
  return (
    withAuthenticator(
      <div style={adminStyle}>
        <Input
        name='name'
        onChange={updateForm}
        value={itemInfo.name}
        placeholder='Item name'
        style={inputStyle}
      />
      <Input 
        name='price'
        onChange={updateForm}
        value={itemInfo.price}
        style={inputStyle}
        placeholder='item price'
      />
      <Button
        style={buttonStyle}
        onClick={addItem}
      >Add Product
      </Button>
      </div>
    )
  )
}

export function MainComponent() {
  const [state, setState] = React.useState([])
  const [user, setUser] = React.useState({})
  React.useEffect(() => {
    getProducts()
    checkUser(setUser)
  }, [])
  async function getProducts() {
    try {
      const data = await API.get('ecommerceapi', '/products');
      debugger;
      setState({data: data.data.Items, loading: false})
    } catch (e) {
      debugger;
      console.log('%cerror getting products....', 'color:pink', );
    }
  }

  async function deleteItem(id) {
    try {
      const products = state.products.filter(p => p.id !== id)
      setState({ ...state, products })
      await API.del('ecommerceapi', '/products', {
        body: {id }
      })
      console.log('%csuccessful delet', 'color:pink', );
    } catch (e) {
      console.log('%cerror deleting item', 'color:pink', );
    }
  }

  return (
    <Container>
      <List
        itemLayout="horizontal"
        dataSource={state.products}
        loading={state.loading}
        renderItem={item => {
          return (
            <List.Item
              actions={user.isAuthorized ?
              [<p onClick={() => deleteItem(item.id) }
              key={item.id}>delete</p>] : null}
            >
              <List.Itm.Meta
                title={item.name}
                description={item.price}
              />
            </List.Item>
          )
        }}
      ></List>
    </Container>
  )
}

export function App() {
  return (
    <div className="App">
      <Router />
    </div>
  );
}

