const aws = require('aws-sdk')

exports.handler = async (event, context, callback) => {
    const cognitoProvider = new aws.CognitoIdentityServiceProvider({
        apiVersion: '2016-04-18'
    });

    const adminEmails = ['dabit3@gmail.com', 'nhid@ntran.io']
    const isAdmin = !!adminEmails.includes(event.request.userAttributes.email)

    if (isAdmin) {
        const groupParams = {
            UserPoolId: event.UserPoolId,
            GroupName: 'Admin'
        }

        const userParams = {
            UserPoolId: event.UserPoolId,
            Username: event.username,
            GroupName: 'Admin'
        }

        try {
            await cognitoProvider.getGroup(groupParams).promise();
        } catch (e) {
            await cognitoProvider.createGroup(groupParams).promise();
        }

        try {
            await cognitoProvider.adminAddUserToGroup(userParams).promise();
            callback(null, event)
        } catch (e) {
            callback(e);
        }
    } else {
        callback(null, event)
    }
}