export const onCreateNote = `subscription OnCreateNote {
    onCreateNote {
        id
        clientId
        name
        description
        completed
    }
}`
;
