import React from 'react';
import logo from './logo.svg';
import './App.css';
import { API } from 'aws-amplify';
import { List, Input, Button } from 'antd';
import 'antd/dist/antd.css';
import { v4 as uuid } from 'uuid'
import { listNotes } from './graphql/queries.js'
import { 
  createNote as CreateNote,
  deleteNote as DeleteNote, 
  updateNote, 
  updateNote as UpdateNote
} from './graphql/mutations.js';
import { onCreateNote } from './graphql/subscriptions';

const CLIENT_ID = uuid();

const initialState = { 
  notes: [],
  loading: true,
  error: false,
  form: { name: '', description: '' },
}
function reducer(state, action) {
  switch(action.type) {
    case 'NEW_NOTE': 
      return { ...state, notes: [...state.notes, action.payload.note]}
    case 'SET_NOTES':
      return { ...state, notes: action.notes, loading: false };
    case 'ERROR':
      return { ...state, loading: false, error: true }
    case 'RESET_FORM': 
      return { ...state, form: initialState.form}
    case 'SET_INPUT':
      return { ...state, form: { ...state.form, [action.payload.name]: action.payload.value }}
    case 'UPDATE_NOTES': 
      return { ...state, notes: action.payload}
    default: 
      return state;
  }
}

const styles = {
  container: { padding: 20 },
  input: { marginBottom: 10},
  item: { textAlign: 'left'},
  p: { color: '#1890ff' }
}

function App() {
  
  const [state, dispatch] = React.useReducer(reducer, initialState)


  React.useEffect(() => {
    fetchNotes();
    const subscription = API.graphql({
      query: onCreateNote
    })
    .subscribe({ 
      next: noteData => {
        // debugger;
        const note = noteData.value.data.onCreateNote
        if (CLIENT_ID === note.clientId) {
          return
        }
        dispatch({ type: 'ADD_NOTE', note })
      }
    })
    return () => subscription.unsubscribe();
  }, [])


  async function fetchNotes () {
    try {
      const notesData = await API.graphql({
        query: listNotes
      })
      dispatch({ type: 'SET_NOTES', notes: notesData.data.listNotes.items })
    } catch (err) {
      console.log('error:', err);
      dispatch({ type: 'ERROR'})
    }
  }

  async function createNote() {
    const { form } = state;
    if (!form.name || !form.description) {
      return alert('please enter a name and descrption')
    }
    const note = {...form, clientId: CLIENT_ID, completed: false, id: uuid() }
    // dispatch({ type: 'ADD_NOTE', note })
    // dispatch({ type: 'RESET_FORM' })
    try {
      console.log('note--', note)
      await API.graphql({
        query: CreateNote,
        variables: { input: note }
      })
      fetchNotes();
      console.log('successfully created note!')
    } catch(err) {
      console.log('error: ', err)
    }
  }

  async function deleteNote({id}) {
    console.log('ID:', id)
    // debugger;
    const index = state.notes.findIndex(n => n.id === id);
    const notes = [
      ...state.notes.slice(0, index),
      ...state.notes.slice(index+1)
    ];

    dispatch({ type: 'SET_NOTES', notes })
    try {
      await API.graphql({
        query: DeleteNote,
        variables: { input: { id }}
      })
      console.log('successfully deleted note!')
    } catch (err) {
      dispatch({ type: 'ERROR'})
      console.log({ err })
    }
  }

  async function updateNote({id, completed}) {
    const index = state.notes.findIndex(n => n.id === id) 
  


    const notes = [ ...state.notes ]
    notes[index].completed = !completed;
    dispatch({ type: 'UPDATE_NOTES', payload: notes})
    try {
      await API.graphql({
        query: UpdateNote,
        variables: { input: { id, completed: !completed }}
      })
      console.log('successfully updated!')
    } catch(err) {
      dispatch({ type: 'ERROR'})
      console.log({err})
    }
  }

  function onChange(e) {
    dispatch({ type: 'SET_INPUT', 
    payload: { name: e.target.name, value: e.target.value }})
  }

  function renderItem (item) {
    return (
      <List.Item 
      style={styles.item}
      actions={[
      <p style={styles.p} onClick={() => deleteNote(item)}>Delete</p>,
      <p style={styles.p} onClick={() => updateNote(item)}>{item.completed ? 'completed' : 'mark completed'}</p>
    ]}
      >
        <List.Item.Meta
          title={item.name}
          description={item.description}
        />
      </List.Item>
    )

  }

  console.log('states.notes...  ', state.notes)
  return (
    <div className="App" style={styles.container}>
        <Input
          onChange={onChange}
          value={state.form.name}
          placeholder="Note Name"
          name="name"
          style={styles.input}
        />
        <Input
          onChange={onChange}
          value={state.form.description}
          placeholder="Note description"
          name='description'
          styles={styles.input}
        />
        <Button
          onClick={createNote}
          type="primary"
        >Create Note</Button>
        <List 
          loading={state.loading}
          dataSource={state.notes}
          renderItem={renderItem}      
        /> 
    </div>
  );
}

export default App;
