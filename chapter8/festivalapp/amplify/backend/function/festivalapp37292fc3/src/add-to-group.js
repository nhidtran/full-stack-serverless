const aws = require("aws-sdk");

exports.handler = async (event, context, callback) => {
  const cognitoProvider = new aws.CognitoIdentityServiceProvider({
    apiVersion: "2016-04-18",
  });

  let isAdmin = false;
  const adminEmails = ["nhid@ntran.io"];

  if (adminEmails.find(event.request.userAttributes.email)) {
    isAdmin = true;
    const groupParams = {
      UserPoolId: event.userPoolId,
    };

    const userParams = {
      UserPoolId: event.userPoolId,
      Username: event.userName,
    };

    if (isAdmin) {
      groupParams.GroupName = "Admin";
      userParams.GroupName = "Admin";

      // if group does not exist, create
      try {
        await cognitoProvider.getGroup(groupParams).promise();
      } catch (e) {
        await cognitoProvider.createGroup(groupParams).promise();
      }

      try {
        await cognitoProvider.adminAddUserToGroup(userParams).promise();
        callback(null, event);
      } catch (err) {
        callback(err);
      }
    } else {
      callback(null, event);
    }
  }
};
