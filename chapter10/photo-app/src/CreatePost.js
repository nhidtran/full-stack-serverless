import React from 'react';
import { Button, Input } from 'antd'
import { v4 as uuid } from 'uuid'
import { createPost } from './graphql/mutations'
import { API, graphqlOperation, Storage } from 'aws-amplify'
import { resolveOnChange } from 'antd/lib/input/Input';

const initialFormState = {
    title: '',
    image: {}
}

function CreatePost({ updateViewState }) {
    const [formState, updateFormState] = React.useState(initialFormState)

    function onChange(key, value) {
        updateFormState({
            ...formState,
            [key]: value
        })
    }

    function setPhoto(e) {
        if (!e.target.files[0]) return
        const file = e.target.files[0]
        updateFormState({
            ...formState,
            image: file
        })
    }

    async function savePhoto() {
        const { title, image } = formState
        debugger;
        if (!title || !image.name) return

        const imageKey = uuid() + formState.image.name.replace(/\s/g, '-').toLowerCase()
        debugger;
        const resStorage = await Storage.put(imageKey, formState.image)
        const post = { title, imageKey }
        console.log('%csuccessful image save...', 'color:pink', resStorage);
        API.graphql(graphqlOperation(createPost, {
            input: post
        }))
        .then(data => {
            console.log('%csuccessful creating post....', 'color:pink', data);
            debugger;
            updateViewState('viewPosts')
        })
        .catch(err => {
            console.log('%cerr...', 'color:red', err);
            debugger;
        })
    }

    return (
        <div>
            <h2 style={heading}>Add Photo</h2>
            <Input
                onChange={e => onChange('title', e.target.value)}
                style={withMargin}
                placeholder="Title"
            />
            <input
                type='file'
                onChange={setPhoto}
                style={button}
            />
            <Button
                style={button}
                type="primary"
                onClick={savePhoto}
            >Save Photo</Button>
        </div>
    )
}

const heading = { margin: '20px 0px' }
const withMargin = { marginTop: 10 }
const button = { marginTop: 10 }

export default CreatePost
