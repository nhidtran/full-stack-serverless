import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Radio } from 'antd';
import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react' 
import Posts from './Posts';
import CreatePost from './CreatePost';

function App () {
  const [viewState, updateViewState] = React.useState('viewPosts')

  React.useEffect(() => {
    console.log('%cviewState changing...', 'color:lime', viewState);
  }, [viewState])

  return (
    <div style={container}>
      <h1>Photo App</h1>
      <Radio.Group
        value={viewState}
        onChange={e => updateViewState(e.target.value)}
      >
        <Radio.Button value="viewPosts">View Posts</Radio.Button>
        <Radio.Button value="addPosts">Add Post</Radio.Button>
      </Radio.Group>
      {
        viewState === 'viewPosts' ? (
          <Posts />
        ) : (
          <CreatePost updateViewState={updateViewState} />
        )
      }
      {/* <AmplifySignOut /> */}
    </div>
  )
}

const container = { 
  width: 500,
  margin: '0 auth',
  padding: 50
}

export default withAuthenticator(App);
