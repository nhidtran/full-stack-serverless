import React from 'react';
import { Auth, Hub } from 'aws-amplify';
import Container from './Container'
import { Button, Form } from './Form'

function Profile() {
        const [user, setUser] = React.useState({})
        React.useEffect(() => {
        checkUser()
        Hub.listen('auth', data => {
            // debugger;
            const { payload } = data;
            if (payload.event === "signOut") {
                setUser(null)
            }
        })
    }, [])

    async function checkUser () {
        await Auth.currentUserPoolUser()
        .then(data => {
            // debugger;
            setUser({
                username: data.username,
                ...data.attributes
            })
        })
        .catch(err => {
            // debugger;
            console.log('error:', err)
        })
    }

    function signOut() {
        Auth.signOut()
        .then(data => console.log('successful signout:', data))
        .catch(err => console.log('Error signing out:', err))
    }

    if (user && user.username && user.email) {
        const { username = "", email = "", phone_number = "" } = user;

        return (
            <Container>
                <h1>Profile...</h1>
                <h2>Username: {username}</h2>
                <h3>Email: {email}</h3>
                <h4>Phone: {phone_number}</h4>
                <Button onClick={signOut}>Sign Out</Button>
            </Container>
        )
    }
    return <Form setUser={setUser} />
}

export default Profile