import { Auth } from 'aws-amplify'
import React from 'react'


const buttonStyles = {
    button: {
        backgroundColor: '#006bfc',
        color: 'white',
        width: 316,
        height: 45,
        fontWeight: '600',
        fontSize: 14,
        cursor: 'pointer',
        border: 'none',
        outline: 'none',
        borderRadius: 3,
        marginTop: '25px',
        boxShadow: '0px 1px 3px rgba(0, 0, 0, .3)'
    }
}

export function Button({ onClick, title, children}) {
    return (
        <button style={buttonStyles.button} onClick={onClick}>
            {children}
        </button>
    )
}

const formStyles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        marginTop: 150,
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        height: 45,
        marginTop: 8,
        width: 300,
        maxWidth: 300,
        padding: '0px 8px',
        fontSize: 16,
        outline: 'none',
        border: 'none',
        borderBottom: '2px solid rgba(0, 0, 0, .3)'
    },
    toggleForm: {
        fontWeight: '600',
        padding: '0px 25px',
        marginTop: '15px',
        marginBottom: 0,
        textAlign: 'center',
        color: 'rgba(0,0,0,0.6)'
    },
    resetPassword: {
        marginTop: '5px'
    },
    anchor: {
        color: '#006bfc'
    }
}



export const ForgotPassword = (props) => {
    return (
        <div style={formStyles.container}>
            <input
                name="username"
                placeholder="Username"
                onChange={e => {e.persist(); props.updateFormState(e)}}
                style={formStyles.input}
            />
            <input
                name="password"
                placeholder="New password"
                type="password"
                onChange={e => {e.persist(); props.updateFormState(e)}}
            />
            {/* <Button onClick={props.forgotPasswordSubmit} title="Save new password" /> */}
        </div>
    )
}


const initialFormState = {
    username: '', password: '', email: '', confirmationCode: '', phone_number: ''
}

export function Form(props) {
    const [formType, updateFormType] = React.useState('signIn')
    const [formState, updateFormState] = React.useState(initialFormState)

    function updateForm(event) {
        const newFormState = {
            ...formState,
            [event.target.name]: event.target.value
        }
        updateFormState(newFormState)
    }

    async function signIn({ username, password }, setUser) {
        try {
            const user = await Auth.signIn(username, password)
            // debugger;
            const userInfo = {username: user.username, ...user.attributes}
            setUser(userInfo)
        } catch (err) {
            console.log('%cERRO:', 'color:red', err);
        }
    }

    async function signUp({ username, password, email}, updateFormType) {
        try {
            await  Auth.signUp({ username, password, attributes: { email }})
            console.log('successful signup!')
            updateFormType('confirmSignUp')
        } catch (err) {
            console.log('error signing up... ', err)
        }
    }

    async function confirmSignUp({ username, confirmationCode }, updateFormType) {
        try {
            await Auth.confirmSignUp(username, confirmationCode)
            updateFormType('signIn')
        } catch (err) {
            console.log('error signing up...', err)
        }
    } 

    async function forgotPassword({ username }, updateFormType) {
        try { 
            await Auth.forgotPassword(username)
            updateFormType('forgotPasswordSubmit')
        } catch (err) {
            console.log('error submitting username to reset password....')
        }
    }

    async function forgotPasswordSubmit({ username, confirmationCode, password}, updateFormType) {
        try {
            await Auth.forgotPasswordSubmit(username, confirmationCode, password)
            updateFormType('signIn')
        } catch (err) {
            console.log('error updating passwrod...', err)
        }
    }

    function renderForm() {
        switch(formType) {
            case 'signUp': 
                return <SignUp 
                  signUp={() => signUp(formState, updateFormType)} 
                  updateFormState={e => updateForm(e)} />
            case 'confirmSignUp':
                return <ConfirmSignUp 
                  confirmSignUp={() => confirmSignUp(formState, updateFormType)}
                  updateFormState={e => updateForm(e)} />
            case 'signIn':
                return <SignIn
                    signIn={() => signIn(formState, props.setUser)}
                    updateFormState={e => updateForm(e)}
                />
            case 'forgotPassword':
                return (
                    <ForgotPassword
                    forgotPassword={() => forgotPassword(formState, updateFormType)}
                    updateFormState={e => updateForm(e)} />
                )
            case 'forgotPasswordSubmit':
                return (
                    <ForgotPassword
                        forgotPasswordSubmit={() => forgotPasswordSubmit(formState, updateFormType)}
                        updateFormState={e => updateForm(e)}
                    />
                )
            default:
                return null;
        }
    }
    return (
        <div>
            {renderForm()}
            {formType === "signUp" && (
                <p style={formStyles.toggleForm}>
                    Already have an account? 
                    <span style={formStyles.anchor} onClick={() => updateFormType('signIn')}>
                      Sign In
                    </span>
                </p>
            )}
            {formType === "signIn" && (
                <>
                <p style={formStyles.toggleForm}>
                    Need an account? <span style={formStyles.anchor} onClick={() => updateFormType('signUp')}>
                        Sign Up
                    </span>
                </p>
                <p style={{ ...formStyles.toggleForm, ...formStyles.resetPassword }}>
                    Forget your password? <span style={formStyles.anchor} onClick={() => updateFormType('forgotPassword')}>
                        Reset Password
                    </span>
                </p>
                </>
            )}
        </div>
    )
}


export const SignIn = ({ signIn, updateFormState }) => {
    return (
        <div style={StyleSheet.container}>
            <input
                name='username'
                onChange={e => {e.persist(); updateFormState(e)}}
                style={formStyles.input}
                placeholder='username'
            />
            <input
                type='password'
                name='password'
                onChange={e => {e.persist(); updateFormState(e)}}
                style={formStyles.input}
                placeholder='password'
            />
            <Button onClick={signIn} title="Sign In">Sign In</Button>
        </div>
    )
}

export const SignUp = ({ updateFormState, signUp }) => {
    return(
        <div style={formStyles.container}>
            <input
                name='username'
                onChenge={e => {e.persist(); updateFormState(e)}}
                style={formStyles.input}
                placeholder="username"
            />
            <input 
                name='password'
                onChange={e => {e.persist(); updateFormState(e)}}
                style={formStyles.input}
                placeholder='password'
            />
            <input
                name='email'
                onChange={e => {e.persist(); updateFormState(e)}}
                style={formStyles.input}
                placeholder='email'
            />
            <Button onClick={signUp} title="Sign Up" />
        </div>
    )
}

export const ConfirmSignUp = (props) => {
    return (
        <div style={formStyles.container}>
            <input
                name='confirmationCode'
                placeholder='Confirmation Code'
                onChange={e => {e.persist(); props.updateFormState(e)}}
                style={formStyles.input}
            />
            <Button onClick={props.ConfirmSignUp} title="Confirm Sign Up "/>
        </div>
    )
}



